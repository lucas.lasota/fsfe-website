<p>Hi <?=$name?>,<br /> 
You have registered an event on https://fsfe.org/events/tools/eventregistration</p>

<p>Below is the list of the information you gave.</p>
<ul>
	<li>Name: <?=$name?></li>
	<li>Email: <?=$email?></li>
	<li>Event Title: <?=$title?></li>
	<li>Location: <?=$location?></li>
</ul>
 
<p>You find all other information in the xml file attached. It will be uploaded
in the next 24 hours. If you like to withdraw your event or in case you like to change some information,
please contact contact@fsfe.org</p>

<p>If you need material about the FSFE and our activities for your event, please
<a href="https://share.fsfe.org/apps/forms/s/Sk2teQXqccbDxcWWW3RKECMq">fill out
the booth preparation form</a> and let us know about it!</a>
 
<p>Thanks,<br />
your FSFE team</p>
